console.clear()

let posX = 0, posY = 0

// helper function to load all assets
// run asynchronously
function preload() {

}

// runs only once at beginning of sketch
// must wait for assets defined in preload() before it executes
function setup() {
    // frameRate sets the duration of draw()
    // frameRate(2)
    createCanvas(windowWidth, windowHeight);
    rectMode(CENTER) // changes where rect() is displayed
    strokeWeight(5) // changes size of stroke
}

// runs continuously in a loop 60x per second by default
function draw() {
    background(75) // sets background of canvas
    fill(255, 0, 0) // fills the shapes
    // noFill() //removes fill
    stroke('yellow') // border of the shape
    translate(width / 2, height / 2) // translate() changes the relative position of the following shape

    const mapX = map(mouseX, 0, width, 0, 500)
    const mapY = map(mouseY, 0, height, 0, 500)
    rect(posX, posY, mapX, mapY)
    // rect(mouseX, mouseY, 500, 500)

    push() // starts new drawing state
        fill(255, 255, 0)
        ellipse(0, 0, 50)
    pop() // ends this block

    // rect(100, 0, 30, 30)
}

function keyPressed () {
    switch (keyCode) {
        case LEFT_ARROW:
            posX -= 10
            break;
        case RIGHT_ARROW:
            posX += 10
            break
    }
}